package com.flipkart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SignUpAndLoginPageForFlipkart {

	static WebDriver driver;
	static Actions actionsForFlipkartPage;

	public SignUpAndLoginPageForFlipkart(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.LINK_TEXT, using = "New to Flipkart? Create an account")
	@CacheLookup
	static WebElement elementForSignUpNewUser;

	@FindBy(how = How.XPATH, using = "//input[@class='_2zrpKA _1dBPDZ']")
	@CacheLookup
	static WebElement elementForSignUpEnterMobileNumber;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'CONTINUE')]")
	@CacheLookup
	static WebElement elementForSignUpContinue;

	@FindBy(how = How.XPATH, using = "//div[4]//input[1]")
	@CacheLookup
	static WebElement elementForSignUpSetPassword;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Signup')]")
	@CacheLookup
	static WebElement elementForSignUpButton;

	@FindBy(how = How.XPATH, using = "//input[@class='_2zrpKA _3v41xv _1dBPDZ']")
	@CacheLookup
	static WebElement elementForLoginEnterPassword;

	@FindBy(how = How.XPATH, using = "//button[@class='_2AkmmA _1LctnI _7UHT_c']//span[contains(text(),'Login')]")
	@CacheLookup
	static WebElement elementForLoginButton;

	@FindBy(how = How.XPATH, using = "//button[@class='_2AkmmA _1LctnI jUwFiZ']")
	@CacheLookup
	static WebElement elementForLoginRequestOtp;

	@FindBy(how = How.XPATH, using = "//button[@class='_2AkmmA _18ls9P _1eFTEo']")
	@CacheLookup
	static WebElement elementForLoginRequestOtpVerify;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Forgot?')]")
	@CacheLookup
	static WebElement elementForLoginForgotPassword;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Contact Us')]")
	@CacheLookup
	static WebElement elementForLoginContactUs;

	// button[@class='_2AkmmA _1LctnI _7UHT_c']//span[contains(text(),'Login')]
	// input[@class='_2zrpKA _3v41xv _1dBPDZ']

	public static void mSignUpForFlipkart(String mobileNumber, String setPassword) {

		int attempts = 0;
		while (attempts < 2) {
			try {
				Thread.sleep(3000);
				// actionsForFlipkartPage=new Actions(driver);
				// actionsForFlipkartPage.moveToElement(elementForLoginButton).perform();
				elementForSignUpNewUser.click();
				break;
			} catch (Exception e) {
			}
			attempts++;
		}

		elementForSignUpEnterMobileNumber.sendKeys(mobileNumber);
		elementForSignUpContinue.click();
		elementForSignUpSetPassword.sendKeys(setPassword);
		elementForSignUpButton.click();
	}

	public static void mLoginForFlipkart(String mobileNumber, String loginPassword) {
		elementForSignUpEnterMobileNumber.sendKeys(mobileNumber);
		elementForLoginEnterPassword.sendKeys(loginPassword);
		elementForLoginButton.click();
	}

	public static void mRequestOtp(String mobileNumber) {
		try {
			elementForSignUpEnterMobileNumber.sendKeys(mobileNumber);
			elementForLoginRequestOtp.click();
			Thread.sleep(12000);
			elementForLoginRequestOtpVerify.click();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void mLoginForgotPassword(String loginMobileNumber) {
		elementForSignUpEnterMobileNumber.sendKeys(loginMobileNumber);
		elementForLoginForgotPassword.click();
		// elementForLoginContactUs.click();
	}

}
