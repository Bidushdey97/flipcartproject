package com.flipkart.pages;

import java.util.Set;

import org.apache.poi.hpsf.Decimal;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class TopOffersImageTestingPage {

	WebDriver driver;
	JavascriptExecutor exe;
	Actions actionForTopOffers;

	public TopOffersImageTestingPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//div[@class='ooJZfD _3FGKd2']//div[@class='_2NTrR2']//div[1]//div[1]//a[1]//div[1]//div[1]//img[1]")
	@CacheLookup
	WebElement elementForTopOffersImage1;

	@FindBy(how = How.XPATH, using = "//div[@class='_2QUpwp']//div[2]//div[1]//a[1]//div[1]//div[1]//img[1]")
	@CacheLookup
	WebElement elementForTopOffersImage2;

	@FindBy(how = How.XPATH, using = "//div[@class='ooJZfD _3FGKd2']//div[3]//div[1]//a[1]//div[1]//div[1]//img[1]")
	@CacheLookup
	WebElement elementForTopOffersImage3;

	@FindBy(how = How.XPATH, using = "//div[@class='_2QUpwp']//div[@class='_2AEDbQ _1V02gy']//*[local-name()='svg']")
	@CacheLookup
	static WebElement elementForTopOffersScrollLeft;

	@FindBy(how = How.XPATH, using = "//input[@class='_2zrpKA _1dBPDZ']")
	@CacheLookup
	static WebElement elementForSignUpEnterMobileNumber;

	@FindBy(how = How.XPATH, using = "//input[@class='_2zrpKA _3v41xv _1dBPDZ']")
	@CacheLookup
	static WebElement elementForLoginEnterPassword;

	@FindBy(how = How.XPATH, using = "//button[@class='_2AkmmA _1LctnI _7UHT_c']//span[contains(text(),'Login')]")
	@CacheLookup
	static WebElement elementForLoginButton;

	@FindBy(how = How.XPATH, using = "//div[@class='_3G9WVX oVjMho']//div[@class='_3aQU3C']")
	@CacheLookup
	static WebElement elementForImage1WebpageSlider;

	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._2doH3V > div._3e7xtJ > div > div:nth-child(2) > div._1HmYoV.hCUpcT.col-12-12 > div > div > div._3ywJNQ > div:nth-child(3)")
	@CacheLookup
	static WebElement elementForImage1SortByPriceLowToHigh;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Price -- High to Low')]")
	@CacheLookup
	static WebElement elementForImage1SortByPriceHighToLow;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Newest First')]")
	@CacheLookup
	static WebElement elementForImage1SortByNewestFirst;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Clear all')]")
	@CacheLookup
	static WebElement elementForImage1ClearingAllFilters;

	@FindBy(how = How.XPATH, using = "//div[@class='_1qKb_B']//select[@class='fPjUPw']")
	@CacheLookup
	static WebElement elementForImage1WebpageMinDropdowns;

	@FindBy(how = How.XPATH, using = "//div[@class='_1YoBfV']//select[@class='fPjUPw']")
	@CacheLookup
	static WebElement elementForImage1WebpageMaxDropdowns;

	@FindBy(how = How.XPATH, using = "//div[@class='_1p7h2j _2irnD_']")
	@CacheLookup
	static WebElement elementForImage1WebpageSupercoinsPriceCheckbox;

	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[5]/div/div")
	@CacheLookup
	static WebElement elementForImage1WebpageDifferentBrandsScrollDown;
	
	//div[contains(text(),'Brand')]
	//*[@id="container"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[5]/div/div
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'143 MORE')]")
	@CacheLookup
	static WebElement elementForImage1WebpageMoreBrandsName;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div:nth-child(1) > div > div > div > section:nth-child(6) > div._3mk-PQ > div > div:nth-child(1) > div > div > label > div._1p7h2j")
	@CacheLookup
	static WebElement elementForImage1WebpageOneBrandCheckbox;
	

	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[6]/div[1]/div")
	@CacheLookup
	static WebElement elementForImage1WebpageCustomerRatings;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[6]/div[2]/div/div[1]/div/div/label/div[1]")
	@CacheLookup
	static WebElement elementForImage1WebpageCustomerRatingsCheckbox;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'_1HmYoV _35HD7C')]//div[2]//div[1]//div[1]//div[1]//a[1]//div[1]//div[1]//div[1]//img[1]")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpage;

	@FindBy(how = How.XPATH, using = "//button[@class='_2AkmmA _2Npkh4 _2MWPVK']")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageAddToCart;
	
	@FindBy(how = How.XPATH, using = "//span[1]//li[1]//div[1]//span[1]")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageBankOfferTAndC;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"swatch-0-size\"]/a")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageProductSize;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[1]/div[2]/div[4]/div[1]/div[2]/div/div/div/div[2]/span")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageViewDetails;
	
	@FindBy(how = How.XPATH, using = "//button[@class='_2AkmmA _2Npkh4 _2kuvG8 _7UHT_c']")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageBuyNow;
	
	@FindBy(how = How.CSS, using = "#productRating_LSTACCFSYR35UAHBTKBFOVG65_ACCFSYR35UAHBTKB_ > div")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageRatingsAndReviews;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[7]/div[1]/div")
	@CacheLookup
	static WebElement elementForImage1WebpageOffers;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[7]/div[2]/div/div[1]/div/div/label/div[1]")
	@CacheLookup
	static WebElement elementForImage1WebpageOffersCheckbox;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[8]/div/div")
	@CacheLookup
	static WebElement elementForImage1WebpageDiscount;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[8]/div[2]/div/div[1]/div/div/label/div[1]")
	@CacheLookup
	static WebElement elementForImage1WebpageDiscountCheckbox;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[9]/div/div")
	@CacheLookup
	static WebElement elementForImage1WebpageAvailability;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[2]/div/div[1]/div/div/div/section[9]/div[2]/div/div/div/div/label/div[1]")
	@CacheLookup
	static WebElement elementForImage1WebpageAvailabilityCheckbox;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-5-12._3KsTU0 > div:nth-child(1) > div > div._2uAjEK > div.keS6DZ > div > div._21PE8N > ul > li:nth-child(2) > div > div")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpagePhoto1;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-5-12._3KsTU0 > div:nth-child(1) > div > div._2uAjEK > div.keS6DZ > div > div._21PE8N > ul > li:nth-child(3) > div > div")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpagePhoto2;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-5-12._3KsTU0 > div:nth-child(1) > div > div._2uAjEK > div.keS6DZ > div > div._21PE8N > ul > li:nth-child(4) > div > div")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpagePhoto3;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[1]/div[2]/div[1]/div[3]/div/div/span/span/span")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageShareOption;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[1]/div[2]/div[1]/div[3]/div/div/div/div[1]/div[2]/div/div/div[1]/img")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageShareFacebookOptionClick;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[1]/div[2]/div[1]/div[3]/div/div/div/div[1]/div[2]/div/div/div[2]/img")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageShareTwitterOptionClick;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"container\"]/div/div[3]/div[1]/div[2]/div[1]/div[3]/div/div/div/div[1]/div[2]/div/div/div[3]/img")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageShareEmailOptionClick;
	
	@FindBy(how = How.CSS, using = "#container > div > div._3Z5yZS.NDB7oB._12iFZG._3PG6Wd > div.ooJZfD._3FGKd2 > div.ooJZfD._2oZ8XT.col-8-12 > div:nth-child(1) > div:nth-child(2) > div > span > div > label > div")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageCompareOptionClick;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-8-12 > div:nth-child(4) > div > div._2b7gqe > div > span")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageKnowMoreOption;
	
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._2yiGUH > div > a > span > span")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageCompare1Button;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-8-12 > div._1HmYoV._35HD7C > div:nth-child(4) > div > div:nth-child(2) > button")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageReadMore;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-8-12 > div._1HmYoV._35HD7C > div:nth-child(4) > div > div:nth-child(2) > div > div:nth-child(1) > div")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageGeneral;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-8-12 > div._1HmYoV._35HD7C > div:nth-child(6) > div > div._1B7BXc > div._36nEq1 > button > span")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageRateProductButton;
	
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-8-12 > div:nth-child(5) > div > div > div._3l12t9 > div._32lKg1 > span")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageViewDeliveryDetails;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-8-12 > div._1HmYoV._35HD7C > div:nth-child(1) > div > div:nth-child(2) > li > a > span._37fzmc")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageViewMoreSellers;
	
	@FindBy(how = How.CSS, using = "#container > div > div.t-0M7P._3GgMx1._2doH3V > div._3e7xtJ > div._1HmYoV.hCUpcT > div._1HmYoV._35HD7C.col-8-12 > div:nth-child(3) > div._3nSGUy > div > div > span:nth-child(7) > li > div > span")
	@CacheLookup
	static WebElement elementForImage1WebpageNewestFirstProductWebpageNoCostEMIOnFlipkartAxisBankCreditCardTAndC;
	
	//*[@id="container"]/div/div[3]/div[2]/div[1]/div[2]/div[1]/div[2]/div/span/div/label/div

	public void mTestingTopOffersImages(String mobileNumber, String loginPassword) {
		try {
			elementForSignUpEnterMobileNumber.sendKeys(mobileNumber);
			elementForLoginEnterPassword.sendKeys(loginPassword);
			elementForLoginButton.click();
			actionForTopOffers = new Actions(driver);
			Thread.sleep(2000);
			exe = (JavascriptExecutor) driver;
			exe.executeScript("arguments[0].scrollIntoView(true);", elementForTopOffersImage1);
			// exe.executeScript("window.scrollBy(0,500)");
			Thread.sleep(2000);
			// actionForTopOffers.moveToElement(elementForTopOffersImage1).click().perform();
			elementForTopOffersImage1.click();
			Thread.sleep(2000);
			exe.executeScript("window.history.go(-1)");
			Thread.sleep(2000);
			exe.executeScript("arguments[0].scrollIntoView(true);", elementForTopOffersImage2);
			// exe.executeScript("window.scrollBy(0,500)");
			Thread.sleep(2000);
			// actionForTopOffers.moveToElement(elementForTopOffersImage2).click().perform();
			elementForTopOffersImage2.click();
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			exe.executeScript("arguments[0].scrollIntoView(true);", elementForTopOffersImage3);
			// exe.executeScript("window.scrollBy(0,500)");
			Thread.sleep(2000);
			// actionForTopOffers.moveToElement(elementForTopOffersImage3).click().perform();
			elementForTopOffersImage3.click();
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			// exe.executeScript("arguments[0].scrollIntoView(true);",
			// elementForTopOffersScrollLeft);
			exe.executeScript("arguments[0].scrollIntoView(true);", elementForTopOffersScrollLeft);
			Thread.sleep(2000);
			elementForTopOffersScrollLeft.click();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void mTestingTopOffersImage1WebPage(String mobileNumber, String loginPassword) {
		try {
			elementForSignUpEnterMobileNumber.sendKeys(mobileNumber);
			elementForLoginEnterPassword.sendKeys(loginPassword);
			elementForLoginButton.click();
			Thread.sleep(2000);
			exe = (JavascriptExecutor) driver;
			exe.executeScript("window.scrollBy(0,250)");
			// exe.executeScript("arguments[0].scrollIntoView(true);",
			// elementForTopOffersImage1);
			Thread.sleep(2000);
			elementForTopOffersImage1.click();
//			Thread.sleep(4000);
//		exe.executeScript("arguments[0].scrollIntoView(true);",elementForImage1WebpageSlider);
			Thread.sleep(2000);

			// exe.executeScript("arguments[0].setAttribute('style', 'right:
			// 30%;')",elementForImage1WebpageSlider);

			actionForTopOffers = new Actions(driver);
			Point point = elementForImage1WebpageSlider.getLocation();
			int xcord = point.getX();
			System.out.println("Position of the webelement from left side is " + xcord + " pixels");
			int ycord = point.getY();
			System.out.println("Position of the webelement from left side is " + ycord + " pixels");
			actionForTopOffers.clickAndHold(elementForImage1WebpageSlider).moveByOffset(xcord + 10, 0).release().build()
					.perform();
			Thread.sleep(2000);
			elementForImage1SortByPriceLowToHigh.click();
			Thread.sleep(2000);
			elementForImage1SortByPriceHighToLow.click();
//			Thread.sleep(3000);
//			elementForImage1SortByNewestFirst.click();
			Thread.sleep(2000);
			elementForImage1ClearingAllFilters.click();
			Thread.sleep(2000);
			// actionForTopOffers.dragAndDropBy(elementForImage1WebpageSlider, xcord+1,
			// ycord).release().build().perform();
			// elementForImage1WebpageSlider.click();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void mTestingImage1WebpageMoreElements() {
		try {
			Select oSelectForMinDropdown = new Select(elementForImage1WebpageMinDropdowns);
			oSelectForMinDropdown.selectByIndex(2);
			Thread.sleep(2000);
			Select oSelectForMaxDropdown = new Select(elementForImage1WebpageMaxDropdowns);
			oSelectForMaxDropdown.selectByIndex(0);
			Thread.sleep(2000);

			if (elementForImage1WebpageSupercoinsPriceCheckbox.isDisplayed()) {
				if (elementForImage1WebpageSupercoinsPriceCheckbox.isEnabled()) {
					elementForImage1WebpageSupercoinsPriceCheckbox.click();
				}
			}

			Thread.sleep(2000);
			
			exe = (JavascriptExecutor) driver;
			exe.executeScript("window.scrollBy(0,300)");
			Thread.sleep(2000);
			elementForImage1WebpageDifferentBrandsScrollDown.click();
			Thread.sleep(2000);
			if(elementForImage1WebpageDifferentBrandsScrollDown.isDisplayed()&&elementForImage1WebpageDifferentBrandsScrollDown.isEnabled())
			{
			if(elementForImage1WebpageOneBrandCheckbox.isDisplayed()&&elementForImage1WebpageOneBrandCheckbox.isEnabled())
			{
			elementForImage1WebpageOneBrandCheckbox.click();
			}
			}
			else {
				elementForImage1WebpageDifferentBrandsScrollDown.click();
			}
			Thread.sleep(2000);
			exe.executeScript("window.scrollBy(0,300)");
			Thread.sleep(2000);
			if(elementForImage1WebpageCustomerRatings.isDisplayed()&&elementForImage1WebpageCustomerRatings.isEnabled())
			{
				if(elementForImage1WebpageCustomerRatingsCheckbox.isDisplayed()&&elementForImage1WebpageCustomerRatingsCheckbox.isEnabled())
				{
					elementForImage1WebpageCustomerRatingsCheckbox.click();
				}
			}
			else {
				elementForImage1WebpageCustomerRatings.click();
			}
			Thread.sleep(2000);
			exe.executeScript("window.scrollBy(0,600)");
			Thread.sleep(2000);
			if(elementForImage1WebpageOffers.isDisplayed()&&elementForImage1WebpageOffers.isEnabled())
			{
			//elementForImage1WebpageOffers.click();
			Thread.sleep(2000);
			elementForImage1WebpageOffersCheckbox.click();
			
			}
			Thread.sleep(2000);
			exe.executeScript("window.scrollBy(0,600)");
			Thread.sleep(2000);
			if(elementForImage1WebpageDiscount.isDisplayed()&&elementForImage1WebpageDiscount.isEnabled())
			{
				elementForImage1WebpageDiscount.click();
				Thread.sleep(2000);
				elementForImage1WebpageDiscountCheckbox.click();
			}
			Thread.sleep(2000);
			exe.executeScript("window.scrollBy(0,600)");
			Thread.sleep(2000);
			if(elementForImage1WebpageAvailability.isDisplayed()&&elementForImage1WebpageAvailability.isEnabled())
			{
				elementForImage1WebpageAvailability.click();
				Thread.sleep(2000);
				elementForImage1WebpageAvailabilityCheckbox.click();
				
			}
			//elementForImage1WebpageDifferentBrandsScrollDown.click();
//			Thread.sleep(3000);
			//elementForImage1WebpageCustomerRatings.click();
//			Thread.sleep(3000);
//			elementForImage1WebpageCustomerRatingsCheckbox.click();
			
			Thread.sleep(2000);
			String parentWindowHandle=driver.getWindowHandle();
			System.out.println(driver.getTitle());
			exe.executeScript("arguments[0].scrollIntoView(true);", elementForImage1SortByNewestFirst);
			Thread.sleep(2000);
			elementForImage1WebpageNewestFirstProductWebpage.click();
			Thread.sleep(4000);
			
			Set<String> mListOfWindows=driver.getWindowHandles();
			for(String windows: mListOfWindows)
			{
				if(!parentWindowHandle.equals(windows))
				{
					driver.switchTo().window(windows);
					System.out.println(driver.getTitle());
//					elementForImage1WebpageNewestFirstProductWebpageBankOfferTAndC.click();
//					Thread.sleep(2000);
//					exe.executeScript("arguments[0].scrollIntoView(true);",elementForImage1WebpageNewestFirstProductWebpageViewDetails);
//					Thread.sleep(2000);
//					elementForImage1WebpageNewestFirstProductWebpageProductSize.click();
//					Thread.sleep(2000);
//					exe.executeScript("window.scrollBy(0,1000)");
					Thread.sleep(3000);
//					elementForImage1WebpageNewestFirstProductWebpageBuyNow.click();
//					//elementForImage1WebpageNewestFirstProductWebpageAddToCart.click();
//					Thread.sleep(2000);
//					Actions actionForNewestFirstProductImages=new Actions(driver);
//					Action actionForNewestFirstProductImage1=actionForNewestFirstProductImages.moveToElement(elementForImage1WebpageNewestFirstProductWebpagePhoto1).build();
//					actionForNewestFirstProductImage1.perform();
//					Thread.sleep(2000);
//					Action actionForNewestFirstProductImage2=actionForNewestFirstProductImages.moveToElement(elementForImage1WebpageNewestFirstProductWebpagePhoto2).build();
//					actionForNewestFirstProductImage2.perform();
//					Thread.sleep(2000);
//					Action actionForNewestFirstProductImage3=actionForNewestFirstProductImages.moveToElement(elementForImage1WebpageNewestFirstProductWebpagePhoto3).build();
//					actionForNewestFirstProductImage3.perform();
//					Thread.sleep(2000);
//					elementForImage1WebpageNewestFirstProductWebpageShareOption.click();
//					Thread.sleep(2000);
//					elementForImage1WebpageNewestFirstProductWebpageShareFacebookOptionClick.click();
//					Thread.sleep(4000);
//					elementForImage1WebpageNewestFirstProductWebpageShareTwitterOptionClick.click();
//					Thread.sleep(4000);
//					elementForImage1WebpageNewestFirstProductWebpageShareEmailOptionClick.click();
		//			Thread.sleep(2000);
					elementForImage1WebpageNewestFirstProductWebpageCompareOptionClick.click();
					Thread.sleep(2000);
					elementForImage1WebpageNewestFirstProductWebpageCompare1Button.click();
					Thread.sleep(2000);
					driver.navigate().back();
					Thread.sleep(2000);
					//elementForImage1WebpageNewestFirstProductWebpageRatingsAndReviews.click();
					//elementForImage1WebpageNewestFirstProductWebpageKnowMoreOption.click();
					exe.executeScript("arguments[0].scrollIntoView(true);",elementForImage1WebpageNewestFirstProductWebpageGeneral);
					Thread.sleep(2000);
					elementForImage1WebpageNewestFirstProductWebpageReadMore.click();
					Thread.sleep(2000);
					exe.executeScript("window.scrollBy(0,350)");
					Thread.sleep(2000);
					elementForImage1WebpageNewestFirstProductWebpageRateProductButton.click();
					Thread.sleep(2000);
					driver.navigate().back();
					Thread.sleep(2000);
					exe.executeScript("arguments[0].scrollIntoView(true);",elementForImage1WebpageNewestFirstProductWebpageBankOfferTAndC);
					Thread.sleep(2000);
					elementForImage1WebpageNewestFirstProductWebpageViewDeliveryDetails.click();
					Thread.sleep(2000);
					exe.executeScript("arguments[0].scrollIntoView(true);",elementForImage1WebpageNewestFirstProductWebpageViewDeliveryDetails);
					Thread.sleep(1000);
					exe.executeScript("window.scrollBy(0,150)");
					Thread.sleep(2000);
					elementForImage1WebpageNewestFirstProductWebpageViewMoreSellers.click();
					Thread.sleep(2000);
					exe.executeScript("arguments[0].scrollIntoView(true);",elementForImage1WebpageNewestFirstProductWebpageRatingsAndReviews);
					Thread.sleep(2000);
					elementForImage1WebpageNewestFirstProductWebpageNoCostEMIOnFlipkartAxisBankCreditCardTAndC.click();
					
					
				}
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
