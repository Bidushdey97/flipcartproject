package com.flipkart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchOptionInFlipcart {
	
	WebDriver driver;
	
	public SearchOptionInFlipcart(WebDriver driver)
	{
		this.driver=driver;
	}
	
	@FindBy(how=How.XPATH,using="//input[@placeholder='Search for products, brands and more']")
	@CacheLookup
	static WebElement elementForSearchOption;
	
	@FindBy(how=How.XPATH,using="//button[@class='vh79eN']//*[local-name()='svg']")
	@CacheLookup
	static WebElement elementForSearchClickOption;
	//button[@class='vh79eN']//*[local-name()='svg']
	//button[@class='vh79eN']
	//button[@class='vh79eN']//*[local-name()='svg']
	@FindBy(how=How.XPATH,using="//input[@class='_2zrpKA _1dBPDZ']")
	@CacheLookup
	static WebElement elementForSignUpEnterMobileNumber;
	
	@FindBy(how=How.XPATH,using="//input[@class='_2zrpKA _3v41xv _1dBPDZ']")
	@CacheLookup
	static WebElement elementForLoginEnterPassword;
	
	@FindBy(how=How.XPATH,using="//button[@class='_2AkmmA _1LctnI _7UHT_c']//span[contains(text(),'Login')]")
	@CacheLookup
	static WebElement elementForLoginButton;
	
	public static void mSearhOption(String mobileNumber,String loginPassword,String mTextToSearch)
	{
		try {
			elementForSignUpEnterMobileNumber.sendKeys(mobileNumber);
			elementForLoginEnterPassword.sendKeys(loginPassword);
			elementForLoginButton.click();
			Thread.sleep(4000);
			elementForSearchOption.sendKeys(mTextToSearch);
			Thread.sleep(7000);
			elementForSearchClickOption.click();
			//elementForSearchClickOption.click();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	

}
