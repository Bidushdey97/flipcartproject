package com.flipcart.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.flipkart.pages.SearchOptionInFlipcart;

public class TestcasesForSearchOption {
	
	WebDriver driver;
	
  @Test
  public void SearchOptionForFlipcart() {
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\Downloads\\chromedriver_win32\\chromedriver.exe");
	  driver=new ChromeDriver();
	  driver.manage().deleteAllCookies();
	  driver.manage().window().maximize();
	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  driver.get("https://www.flipcart.com/");
	  SearchOptionInFlipcart objForSearchOption=PageFactory.initElements(driver, SearchOptionInFlipcart.class);
	  objForSearchOption.mSearhOption("9501747064", "dey19977", "Books");
	  System.out.println("Test successful");
  }
}
