package com.flipcart.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.flipkart.pages.SignUpAndLoginPageForFlipkart;

public class TestcasesForLoginPage {
	
  WebDriver driver;
  @Test
  public void LoginForFlipkart() {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\Downloads\\chromedriver_win32\\chromedriver.exe");
		  driver=new ChromeDriver();
		  driver.manage().deleteAllCookies();
		  driver.manage().window().maximize();
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		  driver.get("https://www.flipcart.com/");
		 SignUpAndLoginPageForFlipkart objForLogin=PageFactory.initElements(driver, SignUpAndLoginPageForFlipkart.class);
		 objForLogin.mLoginForFlipkart("9501747064", "dey19977");
		 System.out.println("Test Successful");
	  
  }
}
