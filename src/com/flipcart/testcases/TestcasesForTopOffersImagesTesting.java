package com.flipcart.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.flipkart.pages.SearchOptionInFlipcart;
import com.flipkart.pages.TopOffersImageTestingPage;

public class TestcasesForTopOffersImagesTesting {

	WebDriver driver;
	TopOffersImageTestingPage objForTopOffersImagesTesting;

	@Test(alwaysRun=true)
	public void TopOffersImagesTesting() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.flipkart.com/");
		objForTopOffersImagesTesting = PageFactory.initElements(driver, TopOffersImageTestingPage.class);
		// objForTopOffersImagesTesting.mTestingTopOffersImages("9501747064","dey19977");
		objForTopOffersImagesTesting.mTestingTopOffersImage1WebPage("9501747064", "dey19977");
		System.out.println("Test successful");
	}

	@Test(dependsOnMethods = {"TopOffersImagesTesting"})
	public void TopOffersImage1WebpageTesting() {
		objForTopOffersImagesTesting = PageFactory.initElements(driver, TopOffersImageTestingPage.class);
		objForTopOffersImagesTesting.mTestingImage1WebpageMoreElements();
	}
}
